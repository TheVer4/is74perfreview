from app import db_holder
from domain.entities.user import User


def get_user_data(id: int) -> User:
    return db_holder.user.get_user_by_user_id(id)


