from infrastructure.DTOs.user_update_dto import UserUpdateDto
from app import db_holder


def update_user_data(id: int, user_update_data: UserUpdateDto):
    db_holder.user.update_user_by_user_id(
        user_id=id,
        position=user_update_data.position,
        password=user_update_data.password,
        tg_name=user_update_data.tg_name,
        email=user_update_data.email,
        avatar=user_update_data.avatar,
        fullname=user_update_data.fullname
    )
