from domain.entities.user import User
from infrastructure.DTOs.user_register_dto import UserRegisterDto
from app import db_holder


def register_user(user_register_dto: UserRegisterDto) -> User:
    email = user_register_dto.email
    db_holder.user.add_user(
        email=email,
        role_id=user_register_dto.role,
        password=user_register_dto.password,
        fullname=user_register_dto.fullname,
        pos_id=user_register_dto.position
    )
    return db_holder.user.get_user_by_email(email)


