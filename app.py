from http.client import HTTPException

from flask import Flask, json

app = Flask(__name__)

from infrastructure.database import db_holder
from infrastructure.controllers.user_controller import user_controller_blueprint
app.register_blueprint(user_controller_blueprint)
from infrastructure.controllers.review_controller import review_controller_blueprint
app.register_blueprint(review_controller_blueprint)


@app.after_request
def apply_caching(response):
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Headers", "*")
    response.headers.add("Access-Control-Allow-Methods", "*")
    return response


@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response


if __name__ == "__main__":
    app.run(debug=True, port=7878)
