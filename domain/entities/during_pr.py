from datetime import datetime

from domain.entities.pr_status import PrStatus
from domain.entities.user import User


class DuringPr:
    def __init__(self,
                 id: int,
                 who_started: User,
                 about_who: User,
                 date_start: datetime,
                 date_end: datetime,
                 status: PrStatus,
                 total_mark: float):
        self.id = id
        self.who_started = who_started
        self.about_who = about_who
        self.date_start = date_start
        self.date_end = date_end
        self.status = status
        self.total_mark = total_mark
