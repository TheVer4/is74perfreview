from datetime import datetime

from domain.entities.position import Position
from domain.entities.roles.role import Role


class User:
    def __init__(self,
                 id: int,
                 role: Role,
                 fullname: str,
                 tg_name: str,
                 email: str,
                 avatar: str,
                 password: str,
                 last_pr: datetime,
                 position: Position):
        self.position = position
        self.last_pr = last_pr
        self.avatar = avatar
        self.role = role
        self.fullname = fullname
        self.tg_name = tg_name
        self.id = id
        self.email = email
        self.password = password

    def __str__(self):
        return "User(id='%s')" % self.id
