from datetime import datetime

from domain.entities.during_pr import DuringPr
from domain.entities.user import User


class Review:
    def __init__(self,
                 id: int,
                 during_pr: DuringPr,
                 who_wrote: User,
                 text: str,
                 mark: float,
                 date: datetime):
        self.id = id
        self.during_pr = during_pr
        self.who_wrote = who_wrote
        self.text = text
        self.mark = mark
        self.date = date
