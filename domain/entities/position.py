from domain.entities.department import Department


class Position:
    def __init__(self,
                 id: int,
                 name: str,
                 department: Department,
                 description: str):
        self.id = id
        self.name = name
        self.department = department
        self.description = description
