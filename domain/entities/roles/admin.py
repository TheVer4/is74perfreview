from domain.entities.roles.role import Role


class Admin(Role):
    def __init__(self):
        self.id = 4
        self.name = 'ADMIN'
