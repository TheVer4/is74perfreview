from domain.entities.roles.role import Role


class Moderator(Role):
    def __init__(self):
        self.id = 2
        self.name = 'MODERATOR'
