from domain.entities.roles.role import Role


class DepLeader(Role):
    def __init__(self):
        self.id = 3
        self.name = 'DEP_LEADER'
