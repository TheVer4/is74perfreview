class Role:
    def __init__(self,
                 id: int,
                 name: str):
        self.id = id
        self.name = name

    def does_have_permission_to(self, level: int):
        return self.id >= level
