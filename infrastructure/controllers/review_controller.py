from flask import Blueprint
from flask_jwt import jwt_required

review_controller_blueprint = Blueprint('review_controller', __name__)


@review_controller_blueprint.route('/add_review', methods=['POST'])
@jwt_required()
async def add_review():
    pass

