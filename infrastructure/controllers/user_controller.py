from flask import request, jsonify, Blueprint
from flask_jwt import jwt_required

from application.commands.delete_user import delete_user
from application.commands.register_user import register_user
from application.commands.update_user_data import update_user_data
from application.queries.get_user_data import get_user_data
from domain.entities.roles.admin import Admin
from infrastructure.DTOs.user_data_dto import UserDataDto
from infrastructure.DTOs.user_register_dto import UserRegisterDto
from infrastructure.DTOs.user_update_dto import UserUpdateDto
from shared.utils.roles_utils import check_permissions

user_controller_blueprint = Blueprint('user_controller', __name__)


@user_controller_blueprint.route('/getUserData', methods=['GET'])
@jwt_required()
def get_user_data():
    id = int(request.args.get("id"))

    user = get_user_data(id)
    return jsonify(UserDataDto(id=user.id,
                               avatar=user.avatar,
                               email=user.email,
                               fullname=user.fullname,
                               role=user.role.name,
                               tg_name=user.tg_name))


@user_controller_blueprint.route('/register_user', methods=['POST'])
@jwt_required()
def register_user():
    check_permissions(3)  # TODO - DEP_LEADER создает только в своем department, ADMIN - где угодно

    req = request.json
    return register_user(UserRegisterDto(
        email=req.email,
        role=int(req.role),
        position=int(req.position),
        fullname=req.fullname,
        password=req.password))





@user_controller_blueprint.route('/updateUserData', methods=['PUT'])
@jwt_required()
def update_user_data():
    id = int(request.args.get("id"))
    check_permissions(id)

    req = request.json

    return update_user_data(id, UserUpdateDto(
        email=req.email,
        avatar=req.avatar,
        fullname=req.fullname,
        password=req.password,
        tg_name=req.tg_name,
        position=int(req.position)))


@user_controller_blueprint.route('/deleteUser', methods=['DELETE'])
@jwt_required()
def delete_user():
    check_permissions(Admin().id)

    id = int(request.args.get("id"))
    return delete_user(id)
