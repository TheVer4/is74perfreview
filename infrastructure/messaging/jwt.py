import datetime

from flask_jwt import JWT
from werkzeug.security import safe_str_cmp

from app import app
from infrastructure.database.handlers.user import get_user_by_email
from shared.utils.users_utils import get_user_by_id_or_current_identity


def authenticate(email, password):
    user = get_user_by_email(email)
    if user and safe_str_cmp(user.password.encode('utf-8'), password.encode('utf-8')):
        return user


def identity(payload):
    user_id = payload['identity']
    return get_user_by_id_or_current_identity(user_id)


app.config['SECRET_KEY'] = 'super-secret'
app.config['JWT_EXPIRATION_DELTA'] = datetime.timedelta(days=1)

jwt = JWT(app, authenticate, identity)
