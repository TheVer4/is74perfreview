class UserRegisterDto:
    def __init__(self,
                 email: str,
                 password: str,
                 role: int,
                 position: int,
                 fullname: str):
        self.fullname = fullname
        self.position = position
        self.role = role
        self.password = password
        self.email = email
