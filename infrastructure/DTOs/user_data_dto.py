from domain.entities.roles.role import Role


class UserDataDto:
    def __init__(self,
                 id: int,
                 avatar: str,
                 email: str,
                 fullname: str,
                 role: str,
                 tg_name: str):
        self.tg_name = tg_name
        self.role = role
        self.fullname = fullname
        self.avatar = avatar
        self.id = id
        self.email = email
