class UserUpdateDto:
    def __init__(self,
                 fullname: str,
                 avatar: str,
                 email: str,
                 tg_name: str,
                 password: str,
                 position: int):
        self.position = position
        self.password = password
        self.tg_name = tg_name
        self.email = email
        self.avatar = avatar
        self.fullname = fullname
