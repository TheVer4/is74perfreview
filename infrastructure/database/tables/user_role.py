from infrastructure.database.db_holder import db


class DB_UserRole(db.Model):
    __tablename__ = 'UserRole'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    role = db.Column(db.String(10), nullable=False, unique=True)

    users = db.relationship('User', backref='UserRole', lazy=True)

    def __init__(self, role: str):
        self.role = role

    def __repr__(self):
        return '<UserRole %r>' % self.id
