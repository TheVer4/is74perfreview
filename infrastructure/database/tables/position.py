from infrastructure.database.db_holder import db


class DB_Position(db.Model):
    __tablename__ = 'Position'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    department_id = db.Column(db.Integer, db.ForeignKey('Department.id'), nullable=False)
    description = db.Column(db.Text)

    users = db.relationship('User', backref='Position', lazy=True)

    def __init__(self,
                 name: str,
                 department_id: int,
                 description: str = None):
        self.name = name
        self.department_id = department_id
        if description:
            self.description = description

    def __repr__(self):
        return '<Position %r>' % self.id
