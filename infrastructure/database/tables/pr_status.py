from infrastructure.database.db_holder import db


class DB_PrStatus(db.Model):
    __tablename__ = 'PrStatus'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    status = db.Column(db.String(20), nullable=False, unique=True)

    during_prs = db.relationship('DuringPr', backref='PrStatus', lazy=True)

    def __init__(self, status: str):
        self.status = status

    def __repr__(self):
        return '<PrStatus %r>' % self.id
