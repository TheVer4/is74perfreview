from datetime import datetime

from infrastructure.database.db_holder import db


class DB_DuringPr(db.Model):
    __tablename__ = 'DuringPr'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    who_started = db.Column(db.Integer, db.ForeignKey('User.id'), nullable=False)
    about_who = db.Column(db.Integer, db.ForeignKey('User.id'), nullable=False)
    date_start = db.Column(db.DateTime, nullable=False)
    date_ended = db.Column(db.DateTime)
    status_id = db.Column(db.Integer, db.ForeignKey('PrStatus.id'), nullable=False)
    total_mark = db.Column(db.Integer)

    reviews = db.relationship('Review', backref='DuringPr', lazy=True)

    def __init__(self,
                 who_started: int,
                 about_who: int,
                 date_start: datetime,
                 status: int,
                 date_ended: datetime = None,
                 total_mark: float = None):
        self.who_started = who_started
        self.about_who = about_who
        self.date_start = date_start
        self.status_id = status
        if date_ended:
            self.date_ended = date_ended
        if total_mark:
            self.total_mark = total_mark

    def __repr__(self):
        return '<DuringPr %r>' % self.id
