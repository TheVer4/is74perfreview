from datetime import datetime

from infrastructure.database.db_holder import db


class DB_Review(db.Model):
    __tablename__ = 'Review'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    during_pr = db.Column(db.Integer, db.ForeignKey('DuringPr.id'),  nullable=False)
    who_wrote = db.Column(db.Integer, db.ForeignKey('User.id'), nullable=False)
    text = db.Column(db.Text, nullable=False)
    mark = db.Column(db.Integer)
    date = db.Column(db.DateTime, nullable=False)

    def __init__(self,
                 during_pr: int,
                 who_wrote: int,
                 text: str,
                 date: datetime,
                 mark: float = None):
        self.during_pr = during_pr
        self.who_wrote = who_wrote
        self.text = text
        self.date = date
        if mark:
            self.mark = mark

    def __repr__(self):
        return '<Review %r>' % self.id
