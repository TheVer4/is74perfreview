from infrastructure.database.db_holder import db


class DB_User(db.Model):
    __tablename__ = 'User'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    role_id = db.Column(db.Integer, db.ForeignKey('UserRole.id'), nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(100), nullable=False)
    fullname = db.Column(db.String(150))
    tg_name = db.Column(db.String(50))
    avatar = db.Column(db.String(600))
    last_pr = db.Column(db.DateTime)
    position_id = db.Column(db.Integer, db.ForeignKey('Position.id'), nullable=False)

    who = db.relationship("DuringPr", backref='DuringPr1', lazy=True, foreign_keys='DuringPr.who_started')
    about = db.relationship("DuringPr", backref='DuringPr2', lazy=True, foreign_keys='DuringPr.about_who')
    reviews = db.relationship('Review', backref='User', lazy=True)

    def __init__(self,
                 email: str,
                 role: int,
                 password: str,
                 fullname: str,
                 position_id: int):
        self.email = email
        self.role_id = role
        self.password = password
        self.fullname = fullname
        self.position_id = position_id

    def __repr__(self):
        return '<User %r>' % self.id
