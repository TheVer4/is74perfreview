from infrastructure.database.db_holder import db


class DB_Department(db.Model):
    __tablename__ = 'Department'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50), nullable=False, unique=True)

    users = db.relationship('Position', backref='Department', lazy=True)

    def __init__(self, name: str):
        self.name = name

    def __repr__(self):
        return '<Department %r>' % self.id
