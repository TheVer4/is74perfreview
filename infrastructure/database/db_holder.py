from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from sqlalchemy import exc
from datetime import datetime

from app import app

URI = 'postgresql://pybacked:admin@localhost:5432/is74pr'


def create_connection():
    app.config['SQLALCHEMY_DATABASE_URI'] = URI
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    return SQLAlchemy(app)


db = create_connection()


db_session = db.session


def begin_session():
    db_session.begin()


begin_session()


def close_session():
    db_session.close()


def add_value_in_table(class_variable):
    try:
        db_session.add(class_variable)
        db_session.commit()
    except exc.IntegrityError as err:
        db_session.rollback()
        print("Строка уже существует")


def convert_to_datetime_by_sec(second: int):
    return datetime.fromtimestamp(second)


from infrastructure.database.tables import department, during_pr, position, pr_status, review, user, user_role


def drop_and_create_all_tables_if_not_exist_any():
    names = [
        "UserRole",
        "User",
        "Review",
        "DuringPr",
        "PrStatus",
        "Position",
        "Department"
    ]
    engine = create_engine(URI)
    exist_names = engine.table_names()
    if not all(elem in exist_names for elem in names):
        db.drop_all()
        db.create_all()


#drop_and_create_all_tables_if_not_exist_any()

from infrastructure.database.handlers import department, during_pr, position, pr_status, review, user, user_role