from domain.entities.department import Department
from infrastructure.database.db_holder import add_value_in_table, db_session
from infrastructure.database.tables.department import DB_Department
from infrastructure.database.tables.position import DB_Position
from infrastructure.database.tables.user import DB_User


def convert_to_entity(db_department: DB_Department) -> Department:
    return Department(
        id=db_department.id,
        name=db_department.name
    )


def add_department(dep_name: str):
    add_value_in_table(DB_Department(dep_name))


def get_department_by_name(dep_name: str):
    return convert_to_entity(db_session
                             .query(DB_Department)
                             .filter(DB_Department.name == dep_name)
                             .first())


def get_departments():
    return [convert_to_entity(dep) for dep in db_session
            .query(DB_Department)
            .all()]


def get_department_by_user_id(user_id: int):
    user_pos_id = db_session \
        .query(DB_User) \
        .filter(DB_User.id == user_id) \
        .first().position_id
    user_dep_id = db_session \
         .query(DB_Position) \
         .filter(DB_Position.id == user_pos_id) \
         .first().department_id
    return convert_to_entity(db_session
                             .query(DB_Department)
                             .filter(DB_Department.id == user_dep_id)
                             .first())


def get_department_by_id(id: int):
    return convert_to_entity(db_session
                             .query(DB_Department)
                             .filter(DB_Department.id == id)
                             .first())
