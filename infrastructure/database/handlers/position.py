from domain.entities.position import Position
from infrastructure.database.db_holder import add_value_in_table, db_session
from infrastructure.database.handlers.department import get_department_by_name, get_department_by_id
from infrastructure.database.tables.position import DB_Position
from infrastructure.database.tables.user import DB_User


def convert_to_entity(db_position: DB_Position) -> Position:
    return Position(
        id=db_position.id,
        name=db_position.name,
        department=get_department_by_id(db_position.department_id),
        description=db_position.description
    )


def add_position_by_department_name(
        position_name: str,
        department_name: str,
        description: str = None):
    dep_id = get_department_by_name(department_name).id
    add_value_in_table(DB_Position(position_name, dep_id, description))


def add_position_by_department_id(
        name: str,
        department_id: int,
        description: str = None):
    add_value_in_table(DB_Position(name, department_id, description))


def get_position_by_name(position_name: str):
    return convert_to_entity(db_session
                             .query(DB_Position)
                             .filter(DB_Position.name == position_name)
                             .first())


def get_position_by_user_id(user_id: int):
    user_pos_id = db_session \
        .query(DB_User) \
        .filter(DB_User.id == user_id) \
        .first().position_id
    return convert_to_entity(db_session
                             .query(DB_Position)
                             .filter(DB_Position.id == user_pos_id)
                             .first())


def get_positions_by_department_id(dep_id: int):
    return convert_to_entity(db_session
                             .query(DB_Position)
                             .filter(DB_Position.department_id == dep_id)
                             .all())


def get_position_by_id(id: int):
    return convert_to_entity(db_session
                             .query(DB_Position)
                             .filter(DB_Position.id == id)
                             .first())


def get_positions():
    return [convert_to_entity(pos) for pos in db_session
            .query(DB_Position)
            .all()]


def update_description_in_position_by_id(
        position_id: int,
        description: str):
    try:
        db_session \
            .query(DB_Position) \
            .filter(DB_Position.id == position_id) \
            .update({DB_Position.description: description})
        db_session.commit()
    except Exception as err:
        db_session.rollback()
        print(err)


def update_description_in_position_by_name(
        position_name: str,
        description: str):
    pos_id = get_position_by_name(position_name).id
    update_description_in_position_by_id(pos_id, description)
