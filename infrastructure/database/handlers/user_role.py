from domain.entities.roles.role import Role
from infrastructure.database.db_holder import add_value_in_table, db_session
from infrastructure.database.tables.user_role import DB_UserRole


def convert_to_entity(db_role: DB_UserRole) -> Role:
    return Role(
        id=db_role.id,
        name=db_role.role
    )


def add_user_role(name: str):
    add_value_in_table(DB_UserRole(name))


def get_role_by_name(role_name: str):
    return convert_to_entity(db_session
                             .query(DB_UserRole)
                             .filter(DB_UserRole.role == role_name)
                             .first())


def get_roles():
    return [convert_to_entity(role) for role in db_session
            .query(DB_UserRole)
            .all()]


def get_role_by_id(id: int):
    return convert_to_entity(db_session
                             .query(DB_UserRole)
                             .filter(DB_UserRole.id == id)
                             .first())
