from domain.entities.pr_status import PrStatus
from infrastructure.database.db_holder import add_value_in_table, db_session
from infrastructure.database.tables.pr_status import DB_PrStatus


def convert_to_entity(db_pr_status: DB_PrStatus) -> PrStatus:
    return PrStatus(
        id=db_pr_status.id,
        name=db_pr_status.status
    )


def add_status(status: str):
    add_value_in_table(DB_PrStatus(status))


def get_statuses():
    return [convert_to_entity(status) for status in db_session
            .query(DB_PrStatus)
            .all()]


def get_status_by_id(id: int):
    return convert_to_entity(db_session
                             .query(DB_PrStatus)
                             .filter(DB_PrStatus.id == id)
                             .first())
