from domain.entities.review import Review
from infrastructure.database.db_holder import add_value_in_table, db_session
from infrastructure.database.handlers.during_pr import get_during_pr_by_id
from infrastructure.database.tables.review import DB_Review
from shared.utils.users_utils import get_user_by_id_or_current_identity


def convert_to_entity(db_review: DB_Review) -> Review:
    return Review(
        id=db_review.id,
        during_pr=get_during_pr_by_id(db_review.during_pr),
        who_wrote=get_user_by_id_or_current_identity(db_review.who_wrote),
        text=db_review.text,
        mark=db_review.mark,
        date=db_review.date
    )


def add_review(
        during_pr: int,
        who_wrote_id: int,
        text: str,
        sec: int,
        mark: float = None):
    date = convert_to_datetime_by_sec(sec)
    add_value_in_table(DB_Review(during_pr, who_wrote_id, text, date, mark))


def get_all_review_about_user_by_during_pr_id(during_pr_id: int):
    return [convert_to_entity(review) for review in db_session
            .query(DB_Review)
            .filter(DB_Review.during_pr == during_pr_id)
            .all()]
