from domain.entities.during_pr import DuringPr
from infrastructure.database.db_holder import add_value_in_table, db_session
from infrastructure.database.handlers.pr_status import get_status_by_id
from infrastructure.database.handlers.user import get_user_by_user_id
from infrastructure.database.tables.during_pr import DB_DuringPr


def convert_to_entity(db_during_pr: DB_DuringPr) -> DuringPr:
    return DuringPr(
        id=db_during_pr.id,
        who_started=get_user_by_user_id(db_during_pr.who_started),
        about_who=get_user_by_user_id(db_during_pr.about_who),
        date_start=db_during_pr.date_start,
        date_end=db_during_pr.date_ended,
        status=get_status_by_id(db_during_pr.status_id),
        total_mark=db_during_pr.total_mark
    )


def add_during_pr(
        who_started_id: int,
        about_who_id: int,
        sec_start: int,
        status_id: int,
        sec_ended: int = None,
        total_mark: float = None):
    date_s = convert_to_datetime_by_sec(sec_start)
    date_e = convert_to_datetime_by_sec(sec_ended)
    add_value_in_table(DB_DuringPr(who_started_id, about_who_id, date_s, status_id, date_e, total_mark))


def get_during_pr_by_id(during_pr_id: int):
    return convert_to_entity(db_session
                             .query(DB_DuringPr)
                             .filter(DB_DuringPr.id == during_pr_id)
                             .first())


def update_status_in_during_pr_by_status_id(during_pr_id: int, status_id: int):
    try:
        db_session \
            .query(DB_DuringPr) \
            .filter(DB_DuringPr.id == during_pr_id) \
            .update({DB_DuringPr.status_id: status_id})
        db_session.commit()
    except Exception as err:
        db_session.rollback()
        print(err)


def update_date_ended_in_during_pr(during_pr_id: int, sec_ended: int):
    date_e = convert_to_datetime_by_sec(sec_ended)
    try:
        db_session \
            .query(DB_DuringPr) \
            .filter(DB_DuringPr.id == during_pr_id) \
            .update({DB_DuringPr.date_ended: date_e})
        db_session.commit()
    except Exception as err:
        db_session.rollback()
        print(err)


def update_total_mark_in_during_pr(during_pr_id: int, total_mark: int):
    try:
        db_session \
            .query(DB_DuringPr) \
            .filter(DB_DuringPr.id == during_pr_id) \
            .update({DB_DuringPr.total_mark: total_mark})
        db_session.commit()
    except Exception as err:
        db_session.rollback()
        print(err)
