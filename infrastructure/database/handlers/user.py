from domain.entities.user import User
from infrastructure.database.db_holder import add_value_in_table, db_session
from infrastructure.database.handlers.position import get_position_by_name, get_position_by_id
from infrastructure.database.handlers.user_role import get_role_by_name, get_role_by_id
from infrastructure.database.tables.position import DB_Position
from infrastructure.database.tables.user import DB_User


def convert_to_entity(db_user: DB_User) -> User:
    return User(
        id=db_user.id,
        email=db_user.email,
        password=db_user.password,
        tg_name=db_user.tg_name,
        fullname=db_user.fullname,
        role=get_role_by_id(db_user.role_id),
        avatar=db_user.avatar,
        last_pr=db_user.last_pr,
        position=get_position_by_id(db_user.position_id)
    )


def add_user_by_role_name_and_position_name(
        email: str,
        role_name: str,
        password: str,
        fullname: str,
        position_name: str):
    role_id = get_role_by_name(role_name).id
    pos_id = get_position_by_name(position_name).id
    add_value_in_table(DB_User(email, role_id, password, fullname, pos_id))


def add_user(
        email: str,
        role_id: int,
        password: str,
        fullname: str,
        pos_id: int):
    add_value_in_table(DB_User(email, role_id, password, fullname, pos_id))


def get_users_by_department_id(department_id: int):
    positions = db_session \
        .query(DB_Position) \
        .filter(DB_Position.department_id == department_id) \
        .all()
    pos_ids = [pos.id for pos in positions]
    return [convert_to_entity(user) for user in db_session
            .query(DB_User)
            .filter(DB_User.position_id.in_(pos_ids))
            .all()]


def get_user_password_by_id(user_id: int):
    return db_session \
        .query(DB_User) \
        .filter(DB_User.id == user_id) \
        .first().password


def get_user_by_email(user_email: str):
    return convert_to_entity(db_session
                             .query(DB_User)
                             .filter(DB_User.email == user_email)
                             .first())


def get_user_by_user_id(user_id: int):
    return convert_to_entity(db_session
                             .query(DB_User)
                             .filter(DB_User.id == user_id)
                             .first())


def get_user_password_by_email(user_email: str):
    return db_session \
        .query(DB_User) \
        .filter(DB_User.id == get_user_by_email(user_email).id) \
        .first().password


def update_position(
        user_id: int,
        position_id: int):
    try:
        db_session \
            .query(DB_User) \
            .filter(DB_User.id == user_id) \
            .update({DB_User.position_id: position_id})
        db_session.commit()
    except Exception as err:
        db_session.rollback()
        print(err)


def update_position_by_position_name(
        user_id: int,
        position_name: str):
    pos_id = get_position_by_name(position_name).id
    update_position(user_id, pos_id)


def update_last_pr(
        user_id: int,
        sec_last_pr: int):
    last_pr_date = convert_to_datetime_by_sec(sec_last_pr)
    try:
        db_session.query(DB_User) \
            .filter(DB_User.id == user_id) \
            .update({DB_User.last_pr: last_pr_date})
        db_session.commit()
    except Exception as err:
        db_session.rollback()
        print(err)


def update_avatar(
        user_id: int,
        avatar: str):
    try:
        db_session \
            .query(DB_User) \
            .filter(DB_User.id == user_id) \
            .update({DB_User.avatar: avatar})
        db_session.commit()
    except Exception as err:
        db_session.rollback()
        print(err)


def update_tg_name(
        user_id: int,
        tg_name: str):
    try:
        db_session \
            .query(DB_User) \
            .filter(DB_User.id == user_id) \
            .update({DB_User.tg_name: tg_name})
        db_session.commit()
    except Exception as err:
        db_session.rollback()
        print(err)


def update_fullname(
        user_id: int,
        fullname: str):
    try:
        db_session \
            .query(DB_User) \
            .filter(DB_User.id == user_id) \
            .update({DB_User.fullname: fullname})
        db_session.commit()
    except Exception as err:
        db_session.rollback()
        print(err)


def update_role(
        user_id: int,
        role_id: int):
    try:
        db_session \
            .query(DB_User) \
            .filter(DB_User.id == user_id) \
            .update({DB_User.role_id: role_id})
        db_session.commit()
    except Exception as err:
        db_session.rollback()
        print(err)


def update_user_by_user_id(
        user_id: int,
        position: int,
        password: str,
        tg_name: str,
        email: str,
        avatar: str,
        fullname: str):
    try:
        db_session \
            .query(DB_User) \
            .filter(DB_User.id == user_id) \
            .update({
                DB_User.position_id: position,
                DB_User.password: password,
                DB_User.tg_name: tg_name,
                DB_User.email: email,
                DB_User.avatar: avatar,
                DB_User.fullname: fullname
            })
        db_session.commit()
    except Exception as err:
        db_session.rollback()
        print(err)



def update_user_role_by_user_id_and_role_name(
        user_id: int,
        new_role_name: str):
    role_id = get_role_by_name(new_role_name).id
    update_role(user_id, role_id)


def update_user_password(
        user_id: int,
        password: str):
    try:
        db_session \
            .query(DB_User) \
            .filter(DB_User.id == user_id) \
            .update({DB_User.password: password})
        db_session.commit()
    except Exception as err:
        db_session.rollback()
        print(err)
