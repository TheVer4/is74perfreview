# API-Документация к серверу IS74-PR

## Перед началом

Важно заметить, что практически все методы, которые можно вызывать требуют авторизации. Сейчас мы подробно опишем весь процесс.
Авторизация на сервере происходит посредством JWT. То есть Вам необходимо обратиться к POST-методу `/auth`, передав в теле запроса JSON-Объект в следующем формате:
```json
{
    "username": "name@example.com",
    "password": "anyPassword"
}
```
> ⚠️ Обратите внимание, что предполагается использование поля `username` только в формате e-mail

> ⚠️ Все запросы типа HTTPS POST  в качестве параметров принимают сериализованный JSON


Обращение к методу `/auth` требует как минимум (at least) 2 заголовка:

| Заголовок | Значение |
| ------ | ------ |
| Content-Type | application/json |
| Content-Length | _<вычисляется отправителем по-факту объёма данных>_ |

В случае успешного запроса, вы получаете ответ следующего формата:
```json
{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzUwNTIzNDIsImlhdCI6MTYzNTA1MjA0MiwibmJmIjoxNjM1MDUyMDQyLCJpZGVudGl0eSI6Mn0.2KVFp3BLNTo6lLYScIiwBsSFWNXMMBksWCzHqspFeTA"
}
```
Где поле `access_token` содержит Ваш токен для последующих обращений к методам, требующих авторизацию. 
##### Излишние подробности
Длина токена: 168 символов. Он состоит из 3-х частей, разделённых точками (`.`), а именно
- заголовок [header] (36 символов)
- полезная нагрузка [payload] (87 символов)
- подпись (данные шифрования) [signature] (43 символа)

Подробнее можете узнать по [ссылке на Хабре](https://habr.com/ru/post/340146/)

### Использование токена
Полученный токен необходимо сохранить в локальном хранилище вашего приложения. При дальнейшем обращении к методам, требующим авторизации теперь нужно добавлять HTTP-заголовок `Authorization`, аналогично схеме `Bearer`
Формат запроса выглядит следующим образом:
```http
GET /protected HTTP/1.1
Authorization: JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZGVudGl0eSI6MSwiaWF0IjoxNDQ0OTE3NjQwLCJuYmYiOjE0NDQ5MTc2NDAsImV4cCI6MTQ0NDkxNzk0MH0.KPmI6WSjRjlpzecPvs3q_T3cJQvAgJvaQAPtk1abC_E
```
> ⚠️ Обратите внимание,  значение заголовка Authorization состоит из двух частей: слова `JWT` и самого токена, части разделяются пробелом.

## Решение возможных проблем (Troubleshoot)
##### При попытке авторизации получен код HTTP-500:
- Вероятнее всего, Вы забыли указать необходимые HTTP-заголовки, в частности `Content-Type`
- Возможно, также, вы использовали неверный тип данных для параметров запроса (например `String` вместо `Int`)
##### Получен код HTTP-400
Вероятнее всего, Вы забыли указать необходимые HTTP-заголовки, в частности `Content-Length`
##### Получен код HTTP-405
Вероятнее всего, Вы обращаетесь используя неправильный метод HTTP. (`/auth` работает через POST)
##### Получен JSON-Объект с кодом HTTP-401
Обычно объект с ошибкой выглядит следующим образом:
```json
{
    "description": "Invalid credentials",
    "error": "Bad Request",
    "status_code": 401
}
```
В таком случае поле `description` содержит описание ошибки, в частности "Invalid credentials" означает о том, что пользователя с такой парой учётных данных найти не удалось, либо введённые вами данные содержат ошибку, а "Signature has expired" означает что нужно повторно пройти авторизацию, т.к. срок жизни предыдущего токена истёк.

## getUserData
- HTTP-метод: `GET`
- Авторизация: Требуется
- Уровень доступа: Доступен всем
- Параметры: Все опциональны

| Параметр | Тип | Required | Описание |
| ------ | ------ | ------ | ------ |
| id | Integer | Optional | ID пользователя, по которому запрашивается информация. Если параметр не указан, возвращается информация по пользователю, который сделал запрос. |

Возвращает объект типа UserData, который выглядит следующим образом:
```json
{
    "id": 1,
    "avatar": "/assets/images/face-6.jpg",
    "email": "jason@ui-lib.com",
    "name": "Jason Alexander",
    "role": "SA",
    "tg_id": 000000000,
    "position_id": 1
}
```

| Поле | Тип | Required | Описание |
| ------ | ------ | ------ | ------ |
| id | Integer | Required | ID пользователя, по которому запрашивается информация. |
| avatar | String | Possible null | Путь, по которому можно найти изображение пользователя на сервере |
| email | String | Required | Почтовый адрес пользователя |
| name | String | Required | Полное имя. Включает Фамилию и имя, разделённые пробелом |
| role | String | Required | Уровень доступа к API |
| tg_id | Integer | Possible null | Идентификатор в Telegram. Используется для связи с _Telegram_(r) |
| position_id | Integer | Required | Идентификатор должности |


## getDepartments
- HTTP-метод: `GET`
- Авторизация: Требуется
- Уровень доступа: Доступен всем
- Параметры: Отсутствуют

Возвращает массив объектов типа Department, который выглядит следующим образом:
```json
{
    "id": 1,
    "department_name": "Отдел разработки"
}
```

| Поле | Тип | Required | Описание |
| ------ | ------ | ------ | ------ |
| id | Integer | Required | ID отдела |
| department_name | String | Required | Название отдела |


## getPositions
- HTTP-метод: `GET`
- Авторизация: Требуется
- Уровень доступа: Доступен всем
- Параметры: Все опциональны

| Параметр | Тип | Required | Описание |
| ------ | ------ | ------ | ------ |
| id | Integer | Optional | ID отдела, по которому запрашивается информация. Если параметр не указан, возвращается список должностей из отдела запросившего пользователя. |

Возвращает массив объектов типа Position, который выглядит следующим образом:
```json
{
    "id": 1,
    "position_name": "Разработчик",
    "description": "Разрабатывает программное обеспечение"
}
```

| Параметр | Тип | Required | Описание |
| ------ | ------ | ------ | ------ |
| id | Integer | Required | ID отдела |
| position_name | String | Required | Наименование должности |
| description | String | Possible null | Описание должности |

## getNotifications
- HTTP-метод: `GET`
- Авторизация: Требуется
- Уровень доступа: Доступен всем
- Параметры: Отсутствуют

Возвращает массив объектов типа Notification, который выглядит следующим образом:
```json
{
    "id": 1,
    "heading": "Сообщение",
    "timestamp": 1570702802,
    "title": "Новое сообщение",
    "subtitle": "Требуется заполнить анткету SR",
}
```

| Параметр | Тип | Required | Описание |
| ------ | ------ | ------ | ------ |
| id | Integer | Required | Уникальный идентификатор уведомления во всей системе |
| heading | String | Required | Тип сообщения |
| timestamp | Integer | Required | Timestamp-время создания уведомления |
| title | String | Required | Заголовок сообщения |
| subtitle | String | Required | Текст сообщения |

## deleteNotification
- HTTP-метод: `POST`
- Авторизация: Требуется
- Уровень доступа: Доступен всем
- Параметры: Все обязательны

| Параметр | Тип | Required | Описание |
| ------ | ------ | ------ | ------ |
| id | Integer | Required | ID удаляемого сообщения |


Возвращает массив объектов типа Notification.

## deleteAllNotifications
- HTTP-метод: `POST`
- Авторизация: Требуется
- Уровень доступа: Доступен всем
- Параметры: Отсутствуют

Возвращает пустой массив либо массив оставшихся объектов типа Notification.

## getUsersRequiredSoonPR
- HTTP-метод: `GET`
- Авторизация: Требуется
- Уровень доступа: Доступен с уровня MANAGER
- Параметры: Отсутствуют

Возвращает массив объектов типа RequiredPR, который выглядит следующим образом:
```json
{
    "userId": 1,
    "image": "/assets/images/face-6.jpg",
    "fullname": "Савелий Шульгин",
    "position": "Маркетолог",
    "department": "Отдел маркетинга",
    "lastPRDate": 1604534400
}
```

| Параметр | Тип | Required | Описание |
| ------ | ------ | ------ | ------ |
| userId | Integer | Required | Уникальный идентификатор пользователя во всей системе |
| image | String | Possible null | Путь, по которому можно найти изображение пользователя на сервере  |
| fullname | String | Required | Полное имя. Включает Фамилию и имя, разделённые пробелом |
| position | String | Required | Название должности, занимаемой пользователем |
| department | String | Required | Название отдела, должность в котором занимает пользователь |
| lastPRDate | Integer | Required | Timestamp-время последнего PR |

## getUsersDuringNowPR
- HTTP-метод: `GET`
- Авторизация: Требуется
- Уровень доступа: Доступен с уровня MANAGER
- Параметры: Отсутствуют

Возвращает массив объектов типа DuringNowPR, который выглядит следующим образом:
```json
{
    "userId": 1,
    "image": "/assets/images/face-6.jpg",
    "fullname": "Савелий Шульгин",
    "position": "Маркетолог",
    "department": "Отдел маркетинга",
}
```

| Параметр | Тип | Required | Описание |
| ------ | ------ | ------ | ------ |
| userId | Integer | Required | Уникальный идентификатор пользователя во всей системе |
| image | String | Possible null | Путь, по которому можно найти изображение пользователя на сервере  |
| fullname | String | Required | Полное имя. Включает Фамилию и имя, разделённые пробелом |
| position | String | Required | Название должности, занимаемой пользователем |
| department | String | Required | Название отдела, должность в котором занимает пользователь |

## getUserByEmailPart
- HTTP-метод: `GET`
- Авторизация: Требуется
- Уровень доступа: Доступен всем
- Параметры: Все обязательны

| Параметр | Тип | Required | Описание |
| ------ | ------ | ------ | ------ |
| email | String | Required | Почтовый адрес искомого пользователя |

Возвращает массив объектов типа UserData

## getUserByNamePart
- HTTP-метод: `GET`
- Авторизация: Требуется
- Уровень доступа: Доступен всем
- Параметры: Все обязательны

| Параметр | Тип | Required | Описание |
| ------ | ------ | ------ | ------ |
| name | String | Required | Часть фамилии и/или имени искомого пользователя |

Возвращает массив объектов типа UserData

# To be continued...