from flask_jwt import current_identity


def check_permissions(level: int):
    current_identity.role.does_have_permission_to(level)

