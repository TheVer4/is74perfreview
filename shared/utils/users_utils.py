from flask_jwt import current_identity

from infrastructure.database.handlers.user import get_user_by_user_id
from shared.exceptions.user_not_found_exception import UserNotFoundException


def get_user_by_id_or_current_identity(id: int):
    id = id
    if id is not None:
        user = get_user_by_user_id(id)
        if user is not None:
            result = user
        else:
            raise UserNotFoundException()
    else:
        result = current_identity
    return result
