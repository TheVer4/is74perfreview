from http.client import HTTPException


class LowPermissionsException(HTTPException):
    code = 403
    description = 'You do not have enough permission to do it.'

