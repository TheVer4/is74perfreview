from http.client import HTTPException


class UserNotFoundException(HTTPException):
    code = 404
    description = 'User with this credentials is unavailable.'
